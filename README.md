# README #

Proj for recruting task.

# Packages #
* BruTile.0.7.4.4
* Common.Logging.2.0.0
* GeoAPI.1.7.2
* Microsoft.Bcl.AsyncInterfaces.1.0.0
* Json.Net.1.0.22
* NetTopologySuite.1.13.2
* NetTopologySuite.IO.1.13.2
* Newtonsoft.Json.4.5.11
* Npgsql.4.1.3.1
* odac.client.x86.1.112.3.20
* odp.net.x86.112.3.20
* ProjNET4GeoAPI.1.3.0.3
* SharpMap.1.1.0
* SharpMap.Data.Layers.HeatLayer.1.1.0
* SharpMap.Data.Providers.OracleSpatial.1.1.0
* SharpMap.UI.1.1.0
* System.Buffers.4.5.0
* System.Data.SQLite.MSIL.1.0.86.0
* System.Memory.4.5.3
* System.Numerics.Vectors.4.5.0
* System.Runtime.CompilerServices.Unsafe.4.6.0
* System.Text.Encodings.Web.4.6.0
* System.Text.Json.4.6.0
* System.Threading.Tasks.Extensions.4.5.3
* System.ValueTuple.4.5.0




