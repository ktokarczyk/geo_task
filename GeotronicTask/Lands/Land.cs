﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotronicTask.Lands
{
    /// <summary>
    /// Single land.
    /// </summary>
    public class Land : ILand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Points { get; set; }
        public List<List<double[]>> LandBorderCoordinates { get; set; }

        public Land()
        {
            Points = 0;
            LandBorderCoordinates = new List<List<double[]>>();
        }
    }
}
