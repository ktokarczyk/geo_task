﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotronicTask.Lands
{
    /// <summary>
    /// Collection of polish lands.
    /// </summary>
    public class LandsCollection
    {
        public List<ILand> Lands = new List<ILand>();

        public LandsCollection()
        {
            Lands.Add(new Land() { Id = 1, Name = "śląskie" });
            Lands.Add(new Land() { Id= 2, Name = "opolskie" });
            Lands.Add(new Land() { Id = 3, Name = "wielkopolskie" });
            Lands.Add(new Land() { Id = 4, Name = "zachodniopomorskie" });
            Lands.Add(new Land() { Id = 5, Name = "świętokrzyskie" });
            Lands.Add(new Land() { Id = 6, Name = "kujawsko-pomorskie" });
            Lands.Add(new Land() { Id = 7, Name = "podlaskie" });
            Lands.Add(new Land() { Id = 8, Name = "dolnośląskie" });
            Lands.Add(new Land() { Id = 9, Name = "podkarpackie" });
            Lands.Add(new Land() { Id = 10, Name = "małopolskie" });
            Lands.Add(new Land() { Id = 11, Name = "pomorskie" });
            Lands.Add(new Land() { Id = 12, Name = "warmińsko-mazurskie" });
            Lands.Add(new Land() { Id = 13, Name = "łódzkie" });
            Lands.Add(new Land() { Id = 14, Name = "mazowieckie" });
            Lands.Add(new Land() { Id = 15, Name = "lubelskie" });
            Lands.Add(new Land() { Id = 16, Name = "lubuskie" });
        }
    }
}
