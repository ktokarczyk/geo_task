﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotronicTask.Lands
{
    /// <summary>
    /// Land interface.
    /// </summary>
    public interface ILand
    {
        int Id { get; set; }
        string Name { get; set; }
        int Points { get; set; }
        List<List<double[]>> LandBorderCoordinates { get; set; }
    }
}
