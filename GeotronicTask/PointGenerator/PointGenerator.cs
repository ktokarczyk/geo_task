﻿using SharpMap.Layers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MyDeserializer = GeotronicTask.GisData.DeserializeJson;

namespace GeotronicTask.PointGenerator
{

    /// <summary>
    /// Get coordinaes from JSON for vector shape polygons
    /// </summary>
    public class PointGenerator
    {
        private readonly Random _random;
        private readonly VecotrKeeper _vecotrKeeper;
        private readonly GeoAPI.Geometries.IGeometryFactory _geometryFactory;
        private readonly MyDeserializer.Deserialize _deserialize;
        private List<VectorLayer> _layerList;

        private List<double[]> _generatedPoints;
        private Lands.LandsCollection _landsCollection;

        /// <summary>
        /// Generate random points, agregate coordinates from GeoJson and printig point on map with secure position.
        /// </summary>
        public PointGenerator()
        {
            _random = new Random();
            _vecotrKeeper = new VecotrKeeper();

            GeoAPI.GeometryServiceProvider.Instance = new NetTopologySuite.NtsGeometryServices();
            _geometryFactory = GeoAPI.GeometryServiceProvider.Instance.CreateGeometryFactory();
            _deserialize = new MyDeserializer.Deserialize();
            _generatedPoints = new List<double[]>();
            _landsCollection = new Lands.LandsCollection();
            _layerList = new List<VectorLayer>();
        }


        /// <summary>
        /// Get geometry point collection. From JSON. 
        /// </summary>
        /// <returns></returns>
        public List<VectorLayer> GetGeometries()
        {
            PrintPointsFromCoordinates();
            return _layerList;
        }

        /// <summary>
        /// Send all generated point in to database.
        /// </summary>
        public void SendPointToDatabase()
        {
            using (var pointContext = new Models.PointContext())
            {
                foreach (var point in _generatedPoints)
                {
                    pointContext.Points.Add(new Models.Point(point));
                }

                pointContext.SaveChanges();
            }
        }

        /// <summary>
        /// Generate random points.
        /// </summary>
        public void GenerateRandomPoints()
        {
            for (int i = 0; i < 1000; i++)
            {
                _generatedPoints.Add(
                    new double[] {
                        GetRandomNumber(14.122884860000056, 24.145783075000065),
                        GetRandomNumber(49.002046518000043, 54.836416667000051),
                        GetRandomNumber(0, 0.0003)
                    });
            }
        }

        /// <summary>
        /// Add coordinats from polygon.
        /// </summary>
        private void PrintPointsFromCoordinates()
        {
            try
            {
                _landsCollection = _deserialize.GetListOfCoordinates();

                for (int i = 0; i < _landsCollection.Lands.Count; i++)
                {
                    foreach (var coordinates in _landsCollection.Lands[i].LandBorderCoordinates)
                    {
                        using (var pointContext = new Models.PointContext())
                        {
                            var points = pointContext.Points.ToArray();
                            var geoCall = new Collection<GeoAPI.Geometries.IGeometry>();

                            foreach (var point in points)
                            {
                                int n = coordinates.Count;

                                if (_vecotrKeeper.IsInside(coordinates, n, point.ToArray()))
                                {
                                    var coord = new GeoAPI.Geometries.Coordinate(
                                            point.x,
                                            point.y,
                                            point.z
                                        );

                                    if (geoCall.All(x => x.Coordinates.FirstOrDefault().Distance(coord) > 0.0003))
                                    {
                                        geoCall.Add(_geometryFactory.CreatePoint(coord));
                                        _landsCollection.Lands[i].Points++;
                                    }
                                }
                            }

                            var layer = new VectorLayer(_landsCollection.Lands[i].Name);
                            layer.DataSource = new SharpMap.Data.Providers.GeometryProvider(geoCall);
                            layer.Style.PointColor = new System.Drawing.SolidBrush(GetRandomColor());
                            _layerList.Add(layer);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Get lands collection.
        /// </summary>
        /// <returns></returns>
        public Lands.LandsCollection GetLandsCollection()
            => _landsCollection;

        /// <summary>
        /// For created random points.
        /// </summary>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        private double GetRandomNumber(double minimum, double maximum)
            => _random.NextDouble() * (maximum - minimum) + minimum;
        

        /// <summary>
        /// Create random color.
        /// </summary>
        /// <returns></returns>
        private Color GetRandomColor()
            => Color.FromArgb(_random.Next(0, 256), _random.Next(0, 256), 0);
        
    }

   
}
