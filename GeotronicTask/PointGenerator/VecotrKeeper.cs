﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotronicTask.PointGenerator
{
    /// <summary>
    /// Secure printing point over the polygon.
    /// </summary>
    public class VecotrKeeper
    {

        public bool OnSegment(double[] p, double[] q, double[] r)
        {
            if (q[0] <= Math.Max(p[0], r[0]) &&
                q[0] >= Math.Min(p[0], r[0]) &&
                q[1] <= Math.Max(p[1], r[1]) &&
                q[1] >= Math.Min(p[1], r[1]))
            {
                return true;
            }
            return false;
        }

        public int Orientation(double[] p, double[] q, double[] r)
        {
            var val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1]);

            if (val == 0.0)
            {
                return 0; 
            }
            return (val > 0.0) ? 1 : 2; // clock or counterclock wise 
        }

        public bool DoIntersect(double[] p1, double[] q1, double[] p2, double[] q2)
        {
            // Find the four orientations needed for  
            // general and special cases 
            int o1 = Orientation(p1, q1, p2);
            int o2 = Orientation(p1, q1, q2);
            int o3 = Orientation(p2, q2, p1);
            int o4 = Orientation(p2, q2, q1);

            if (o1 != o2 && o3 != o4)
            {
                return true;
            }

            if (o1 == 0 && OnSegment(p1, p2, q1))
            {
                return true;
            }

            if (o2 == 0 && OnSegment(p1, q2, q1))
            {
                return true;
            }

            if (o3 == 0 && OnSegment(p2, p1, q2))
            {
                return true;
            }

            if (o4 == 0 && OnSegment(p2, q1, q2))
            {
                return true;
            }

            return false;
        }

        public bool IsInside(List<double[]> polygon, int n, double[] p)
        {
            // There must be at least 3 vertices in polygon[] 
            if (n < 3)
            {
                return false;
            }

            // Create a point for line segment from p to 1k
            double[] extreme = new double[] { 1000.0, p[1] };


            int count = 0, i = 0;
            do
            {
                int next = (i + 1) % n;

                if (DoIntersect(polygon[i],
                                polygon[next], p, extreme))
                {

                    if (Orientation(polygon[i], p, polygon[next]) == 0)
                    {
                        return OnSegment(polygon[i], p,
                                         polygon[next]);
                    }
                    count++;
                }
                i = next;
            } while (i != 0);

            return (count % 2 == 1);
        }
    }
}
