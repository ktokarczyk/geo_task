﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeotronicTask.DbCreate
{
    public class Database
    {
        private readonly string _connectionString;

        public Database()
        {
            _connectionString =
                "User ID=postgres;" +
                "Password=32167;" +
                "Server=localhost;" +
                "Port=1444;" +
                "Database=Geo_Task;";
        }


        /// <summary>
        /// Create database.
        /// </summary>
        public void CreateDatabase()
        {
            string connStr = "Server=localhost;Port=1444;User Id=postgres;Password=32167;";
            var conn = new NpgsqlConnection(connStr);
            var sql = File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\data\\createdatabase.sql");
            var sqlCommand = new NpgsqlCommand(sql, conn);
            conn.Open();
            sqlCommand.ExecuteNonQuery();
            conn.Close();

            AddPostGisExtention();
        }

        /// <summary>
        /// Add postgis extention.
        /// </summary>
        private void AddPostGisExtention()
        {
            ExecuteCommand(@"CREATE EXTENSION postgis;");
        }

        /// <summary>
        /// Create point table.
        /// </summary>
        public void CreateTable()
        {
            var sql = File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\data\\createtable.sql");

            if (sql != null)
            {
                ExecuteCommand(sql);
            }
        }

        /// <summary>
        /// Execute command non query
        /// </summary>
        /// <param name="sqlCommand"></param>
        public void ExecuteCommand(string sqlCommand)
        {
            try
            {
                using (var conn = new NpgsqlConnection(_connectionString))
                {
                    var command = new NpgsqlCommand(
                        sqlCommand,
                        conn
                    );

                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


    }
}
