﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GeotronicTask.GisData.MultiPolygon
{

    /// <summary>
    /// Base clase to deserialize geoJson file.
    /// </summary>
    public class GeometryCollection
    {
        public string type { get; set; }
        public List<MultiPolygon> geometries { get; set; }
    }

    public class MultiPolygon 
    {
        public string type { get; set; }
        public List<List<List<double[]>>> coordinates { get; set; }
    }

}


namespace GeotronicTask.GisData.Polygon
{
    public class GeometryCollection
    {
        public string type { get; set; }
        public List<Polygon> geometries { get; set; }
    }
   
    /// <summary>
    /// Poligon class for deserialize GeoJson file.
    /// </summary>
    public class Polygon
    {
        public string type { get; set; }
        public List<List<double[]>> coordinates { get; set; }
    }
}