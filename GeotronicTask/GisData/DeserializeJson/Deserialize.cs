﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeotronicTask.GisData;

using Multi = GeotronicTask.GisData.MultiPolygon;
using Single = GeotronicTask.GisData.Polygon;

namespace GeotronicTask.GisData.DeserializeJson
{
    public class Deserialize
    {
        private Lands.LandsCollection _landsCollection;

        public Deserialize()
        {
            _landsCollection = new Lands.LandsCollection();
        }

        /// <summary>
        /// Create and get list of coordinates from Json.
        /// </summary>
        public Lands.LandsCollection GetListOfCoordinates()
        {
            AgreagateJsonMultiPolyCoordinates();
            AgreagateJsonSinglePolyCoordinates();

            return _landsCollection;
        }

        /// <summary>
        /// Agreagate coordinates to single collection.
        /// </summary>
        private void AgreagateJsonMultiPolyCoordinates()
        {
            try
            {
                var deserializeMultiPolygon = GetMultiPolygon();

                if (deserializeMultiPolygon == null)
                {
                    throw new Exception("MultyPolygon is null reference.");
                }

                foreach (var multipoligon in deserializeMultiPolygon.geometries)
                {
                    foreach (var listOfList in multipoligon.coordinates)
                    {
                        foreach (var listin in listOfList)
                        {
                            _landsCollection.Lands.FirstOrDefault(x => x.Name == "śląskie").LandBorderCoordinates.Add(listin);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Agreagate coordinates to single collection.
        /// </summary>
        private void AgreagateJsonSinglePolyCoordinates()
        {
            try
            {
                var deserializePolygon = GetPolygon();

                if (deserializePolygon == null)
                {
                    throw new Exception("Polygon is null reference.");
                }

                for (int i = 0; i < deserializePolygon.geometries.Count; i++)
                {
                    foreach (var listOfList in deserializePolygon.geometries[i].coordinates)
                    {
                        _landsCollection.Lands.FirstOrDefault(x => x.Id == i + 2).LandBorderCoordinates.Add(listOfList);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        

        /// <summary>
        /// Get coordinaters from GeoJson for MultiPolygon
        /// </summary>
        private Multi.GeometryCollection GetMultiPolygon()
        {
            try
            {
                string jsonPath = $"{AppDomain.CurrentDomain.BaseDirectory}\\data\\slask.json";

                Multi.GeometryCollection geometryCollection;

                using (StreamReader r = new StreamReader(jsonPath))
                {
                    string json = r.ReadToEnd();

                    geometryCollection = JsonConvert.DeserializeObject<Multi.GeometryCollection>(json);
                }

                return geometryCollection;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }

        }

        /// <summary>
        /// Get coordinate from GeoJson for Polygons
        /// </summary>
        /// <param name="jsonPath"></param>
        /// <returns></returns>
        private Single.GeometryCollection GetPolygon()
        {
            try
            {
                string jsonPath = $"{AppDomain.CurrentDomain.BaseDirectory}\\data\\Wojewodztwa.json";

                Single.GeometryCollection geometryCollection;

                using (StreamReader r = new StreamReader(jsonPath))
                {
                    string json = r.ReadToEnd();

                    geometryCollection = JsonConvert.DeserializeObject<Single.GeometryCollection>(json);
                }

                return geometryCollection;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


    }





}
