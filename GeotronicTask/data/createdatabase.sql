﻿-- create database
CREATE DATABASE "Geo_Task"
    WITH OWNER = postgres
    ENCODING = 'UTF8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;