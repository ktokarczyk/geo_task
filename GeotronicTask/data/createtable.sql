
-- create table
CREATE OR REPLACE FUNCTION create_point ()
  RETURNS void AS
$func$
BEGIN
   IF EXISTS (SELECT FROM pg_catalog.pg_tables 
              WHERE  schemaname = 'public'
              AND    tablename  = 'point') THEN
      RAISE NOTICE 'Table public.point already exists.';
   ELSE
      CREATE TABLE public.point 
	  (
		id_point  SERIAL PRIMARY KEY,
		x double precision NOT NULL,
		y double precision NOT NULL,
		z double precision NOT NULL
		);	
   END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_point();  
