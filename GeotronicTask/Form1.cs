﻿using Newtonsoft.Json;
using SharpMap.Layers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/// <summary>
/// data folder should be copied to output (Debug / Release)
/// </summary>

namespace GeotronicTask
{
    public partial class Form1 : Form
    {

        private PointGenerator.PointGenerator _pointGenerator;

        public Form1()
        {
            InitializeComponent();


            Load += Form1_Load;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _pointGenerator = new PointGenerator.PointGenerator();

            CreateMap();
            CreatePointCoverLands();
            CreateDataView();

            mapBox1.Map.ZoomToExtents();
            mapBox1.Refresh();
            mapBox1.ActiveTool = SharpMap.Forms.MapBox.Tools.Pan;
        }

        /// <summary>
        /// Create map and set in to MapBox
        /// </summary>
        private void CreateMap()
        {
            ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory ctFact = 
                new ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory();
            
            SharpMap.Layers.VectorLayer vlay = new SharpMap.Layers.VectorLayer("Wojewodztwa");
            vlay.DataSource = new SharpMap.Data.Providers.ShapeFile($"{AppDomain.CurrentDomain.BaseDirectory}\\data\\Wojewodztwa.shp", true);
            vlay.CoordinateTransformation = 
                ctFact.CreateFromCoordinateSystems(
                    ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84, 
                    ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator
                    );

            mapBox1.Map.Layers.Add(vlay);
        }

        /// <summary>
        /// Create point and print in to map.
        /// </summary>
        private void CreatePointCoverLands()
        {
            var vectorLayers = _pointGenerator.GetGeometries();
            ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory ctFact = 
                new ProjNet.CoordinateSystems.Transformations.CoordinateTransformationFactory();

            foreach (var layer in vectorLayers)
            {
                layer.CoordinateTransformation = 
                    ctFact.CreateFromCoordinateSystems(
                        ProjNet.CoordinateSystems.GeographicCoordinateSystem.WGS84, 
                        ProjNet.CoordinateSystems.ProjectedCoordinateSystem.WebMercator
                        );

                mapBox1.Map.Layers.Add(layer);
            }
        }

        /// <summary>
        /// Set DataSource for GridDataView.
        /// </summary>
        private void CreateDataView()
        {
            var data = _pointGenerator.GetLandsCollection().Lands;
            dataGridView1.DataSource = data;
            dataGridView1.Refresh();
        }

        /// <summary>
        /// Run script to create database.
        /// </summary>
        private void CreateDB()
        {
            DbCreate.Database db = new DbCreate.Database();
            db.CreateDatabase();
        }

        /// <summary>
        /// Run script to create table.
        /// </summary>
        private void CreateTable()
        {
            DbCreate.Database db = new DbCreate.Database();
            db.CreateTable();
        }

        private void GeneratePointAndSaveDb()
        {
            _pointGenerator.GenerateRandomPoints();
            _pointGenerator.SendPointToDatabase();
        }
    }
}
