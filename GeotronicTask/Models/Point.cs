﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GeotronicTask.Models
{

    /// <summary>
    /// For entity database.
    /// </summary>
    [Table("point")]
    public class Point
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id_point { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }

        public Point()
        {

        }

        public Point(double[] points)
        {
            x = points[0];
            y = points[1];
            z = points[2];
        }

        public double[] ToArray()
            => new double[] {
                x,
                y,
                z
            };
    }
}
